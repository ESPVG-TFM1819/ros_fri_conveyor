FROM ros:kinetic-ros-base-xenial

RUN apt-get update \
    && apt-get install -y --no-install-recommends --allow-unauthenticated \
        git \
        wget \
    && apt-get autoclean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*

RUN cd /usr/local/src \ 
    && wget https://cmake.org/files/v3.12/cmake-3.12.1.tar.gz \
    && tar xvf cmake-3.12.1.tar.gz \ 
    && cd cmake-3.12.1 \
    && ./bootstrap \
    && make \
    && make install \
    && cd .. \
    && rm -rf cmake*

COPY . /root/src/conveyor_tech
WORKDIR /root