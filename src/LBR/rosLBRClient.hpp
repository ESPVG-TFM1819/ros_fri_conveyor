

#include <ros/ros.h>
#include <friLBRClient.h>

using namespace KUKA::FRI;

namespace lbr {
	class rosLBRClient : public LBRClient {
		private:
		public:
			virtual void onStateChange(ESessionState oldState, ESessionState newState);
			virtual void monitor();
			virtual void waitForCommand();
			virtual void command();
	};
}
