

#include <stdio.h>

#include "rosLBRClient.hpp"

namespace lbr {

		void rosLBRClient::onStateChange(ESessionState oldState, ESessionState newState) {}
		void rosLBRClient::monitor() {}
		void rosLBRClient::waitForCommand() {}
		void rosLBRClient::command() {}
}
