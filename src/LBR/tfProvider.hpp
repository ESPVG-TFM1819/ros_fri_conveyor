
#include <ros/ros.h>
#include "geometry_msgs/Pose2D.h"
#include "geometry_msgs/Transform.h"

#include <friTransformationClient.h>

using namespace KUKA::FRI;

namespace lbr {
    class tfProvider : public KUKA::FRI::TransformationClient {
		private:	
    		geometry_msgs::Pose2D _lastPose;

		public:
			void setLastPose(geometry_msgs::Pose2D pose);
			void setTransformation(geometry_msgs::Transform trf);
			virtual void provide();
    };
}

