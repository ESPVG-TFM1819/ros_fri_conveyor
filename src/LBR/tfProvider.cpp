
#include "tfProvider.hpp"

namespace lbr {

    void tfProvider::setLastPose(geometry_msgs::Pose2D pose) {
        _lastPose = pose;
    }

    void tfProvider::provide() {
		printf ("client quality: %d\n", getConnectionQuality() );
       
		if (ros::ok()) {
    	//	ros::spinOnce();
			geometry_msgs::Transform trf;
			trf.translation.x = _lastPose.x;
			trf.translation.y = _lastPose.y;

            //yaw
            float cy = std::cos(0 * 0.5);
            float sy = std::sin(0 * 0.5);
            //pitch
            float cp = std::cos(0 * 0.5);
            float sp = std::sin(0 * 0.5);
            //roll
            float cr = std::cos(_lastPose.theta * 0.5);
            float sr = std::sin(_lastPose.theta * 0.5);

			trf.rotation.w = cy;
			trf.rotation.x = 0;
			trf.rotation.y = 0;
			trf.rotation.z = sy;

            setTransformation(trf);
        } else {
			double mocktf[3][4];
        	printf("mocked transformation writen\n");
			TransformationClient::setTransformation("dynFrame", mocktf, getTimestampSec(), getTimestampNanoSec());
		}
    }

    void tfProvider::setTransformation(geometry_msgs::Transform trf) {
        geometry_msgs::Vector3 trans = trf.translation;
        geometry_msgs::Quaternion rot = trf.rotation;

        /// math explanation https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation
        double transformationMatrix[3][4];
        transformationMatrix[0][0] = std::pow(rot.x, 2) + std::pow(rot.y, 2) - std::pow(rot.z, 2) - std::pow(rot.w, 2);
        transformationMatrix[1][0] = 2*rot.y*rot.z + 2*rot.x*rot.w;
        transformationMatrix[2][0] = 2*rot.y*rot.w - 2*rot.x*rot.z;
        
        transformationMatrix[0][1] = 2*rot.y*rot.z - 2*rot.x*rot.w;
        transformationMatrix[1][1] = std::pow(rot.x, 2) - std::pow(rot.y, 2) + std::pow(rot.z, 2) - std::pow(rot.w, 2);
        transformationMatrix[2][1] = 2*rot.z*rot.w + 2*rot.x*rot.y;
        
        transformationMatrix[0][2] = 2*rot.y*rot.w + 2*rot.x*rot.z;
        transformationMatrix[1][2] = 2*rot.z*rot.w - 2*rot.x*rot.y;
        transformationMatrix[2][2] = std::pow(rot.x, 2) - std::pow(rot.y, 2) - std::pow(rot.z, 2) + std::pow(rot.w, 2);

        transformationMatrix[0][3] = trans.x;
        transformationMatrix[1][3] = trans.y;
        transformationMatrix[2][3] = trans.z;
        
        TransformationClient::setTransformation("dynFrame", transformationMatrix, getTimestampSec(), getTimestampNanoSec());	
		//printf("transformation writen (%.2f, %.2f, %.2f)\n", trans.x, trans.y, );
    }
}
