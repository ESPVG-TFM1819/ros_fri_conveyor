
#include "stdio.h"

#include "friUdpConnection.h"
#include "friClientApplication.h"

#include "tfProvider.hpp"
#include "rosLBRClient.hpp"

#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include "conveyor_tech/lbrInfo.h"
#include "conveyor_tech/appState.h"

using namespace KUKA::FRI;
using namespace lbr;

//rosLBRClient client;
tfProvider tf;
LBRClient client;

ros::Publisher _encoder;
ros::Subscriber _piecePos;

void setLastPose(geometry_msgs::Pose2D pose) {
	tf.setLastPose(pose);
}

UdpConnection udp(1000);
ClientApplication app(udp, client, tf);

int main(int argc, char** argv) {

	ros::init(argc, argv, "lbr_client");
	ros::NodeHandle handle;
	
	_encoder = handle.advertise<std_msgs::Float32>("encoder_data", 10, true);
	ROS_INFO("Published - encoder_data");

	_piecePos = handle.subscribe("PiecePose", 50, setLastPose);
	ROS_INFO("Subscrived - PiecePose");

	ROS_INFO("lbr_client initialized");
	/**/

    app.connect(30200, NULL);

	std_msgs::Float32 value;
	while (app.step() && ros::ok()) {
		_encoder.publish(value);
		ros::spinOnce();
		value.data = client.robotState().getDigitalIOValue("Conveyors.Conveyor01_Pos");
		printf("value %.3f\n", value.data);
	}
	ros::shutdown();
    app.disconnect();
}
