/**
 *		
 *		\file: udp_server.cpp
 *		\author: Pablo Morales
 *		\date: Dic 12, 2018
 */

#include <stdio.h>

#include "udp_server.hpp"
#include "ros/ros.h"
#include "geometry_msgs/Pose2D.h"

using namespace cvision;
using namespace boost::asio;
int step = 1; 

udp_server::udp_server(boost::asio::io_service& io_service) : socket_(io_service, udp::endpoint(udp::v4(), 3000)) {
	start_receive();
}

geometry_msgs::Pose2D udp_server::get_pose(){
	return _pose;
}

void udp_server::start_receive() {
	printf("Start receiving data.... \n");
	socket_.async_receive_from(boost::asio::buffer(recv_buffer_), remote_endpoint_, boost::bind(&udp_server::handle_receive, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
	printf("receiving data in progess... \n");
}

void  udp_server::handle_receive(const boost::system::error_code& error, std::size_t) {
	if (!error || error == error::message_size) {
		//printf("Data received : %s \n", std::string(recv_buffer_.begin(), recv_buffer_.size()).c_str());

		float values[3];
		int index = 0;
		std::vector<char> buffer;
		for (auto i = recv_buffer_.begin(); i != recv_buffer_.end(); i++) {					
			if (*i == ';' || i == recv_buffer_.end()) {
				values[index] = strtof(buffer.data(),NULL);
				index++;
				buffer.clear(); 
			} else {
				buffer.push_back(*i);
			}
		}
		_pose.x = values[0];
		_pose.y = values[1];
		_pose.theta = values[2];
		printf("Pose [%.3f, %.3f, %.3f]\n", values[0], values[1], values[2]);
		recv_buffer_.assign(0);
		start_receive();
	} else {
		printf("ERROR : %s", std::string(recv_buffer_.begin(), recv_buffer_.size()).c_str());
	}

}

