/**!
 *		
 *		\file: CVision.cpp
 * 		\author: Adrià Terrades
 *		\date: Oct 18, 2018
 *		\author: Pablo Morales
 *		\date: Dic 12, 2018
 */

#include <iostream>
#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>

using boost::asio::ip::udp;

void dispose();

class udp_server
{
	public:
		udp_server(boost::asio::io_service& io_service) : socket_(io_service, udp::endpoint(udp::v4(), 3000))
		{
			start_receive(); 
		}
	private:
		void start_receive()
		{
			std::cout <<"receiving data.... \n";
			socket_.async_receive_from( boost::asio::buffer(recv_buffer_), remote_endpoint_, boost::bind(&udp_server::handle_receive, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
			std::cout <<"receiving data in progess \n";
			
		}

		void handle_receive(const boost::system::error_code& error, std::size_t /*bytes_transferred*/)
		{	
			if (!error || error == boost::asio::error::message_size)
			{
				std::cout <<"Data : " << std::string(reinterpret_cast<const char*>(recv_buffer_.data()),recv_buffer_.size()) << std::endl;;
				
				start_receive();
			}else 
			{
				std::cout <<"ERROR : " << std::string(reinterpret_cast<const char*>(recv_buffer_.data()),recv_buffer_.size()) << std::endl;;
			}
		}
		
	udp::socket socket_;
	udp::endpoint remote_endpoint_;
	boost::array<char, 32> recv_buffer_;
};


int main() {

	//Initialize phase
	try {
		std::cout <<"1... \n";
		boost::asio::io_service io_service;
		std::cout <<"2... \n";
		udp_server server(io_service); 
		std::cout <<"3... \n";
		io_service.run(); 	

	} catch(const std::exception& e) {
		
		std::cerr << e.what() << std::endl;

	}

	return 0; 
}	


