/**!
 *		
 *		\file: CVision.cpp
 * 		\author: Adrià Terrades
 *		\author: Pablo Morales
 *		\date: Dic 12, 2018
 */

#include <iostream>
#include <boost/bind.hpp>
#include <boost/thread.hpp>

#include "udp_server.hpp"
#include "ros/ros.h"
#include "geometry_msgs/Pose2D.h"

ros::Publisher _posePbl;
using boost::asio::ip::udp;

using namespace cvision;
void dispose();

int main(int argc, char **argv) {

	//Initialize phase
	try {
		ros::init(argc, argv, "CVision");	//Initializes ROS Node
		
		ROS_INFO("CVision Started...");
		ros::NodeHandle handle;
		
		_posePbl = handle.advertise<geometry_msgs::Pose2D>("last_pose", 500, false);
		ROS_INFO("Published - last_pose");
	
	} catch(const std::exception& e) {
		ROS_ERROR("Inintializon error");
		std::cerr << e.what();
		dispose();
		return 1;
	}

	//Run phase
	try {
		boost::asio::io_service io_service;
		udp_server server(io_service); 
		
		while (ros::ok()) {
			io_service.run_one();
			_posePbl.publish(server.get_pose());
			ROS_INFO("pose");
			ros::spinOnce();
			//ROS_INFO("I am ALIVE 2"); 
		}
	} catch(const std::exception& e) {
		ROS_ERROR("Runtime error");
		std::cerr << e.what();
		dispose();
		return 1;
	}

	//Dispose phase
	dispose();
	return 0;
}

void dispose() {
	ROS_INFO("Shutting down...");
	ros::shutdown();
}
