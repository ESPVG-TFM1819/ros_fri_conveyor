/**!
 *		
 *		\file: CVision.cpp
 * 		\author: Adrià Terrades
 *		\date: Oct 18, 2018
 *		\author: Pablo Morales
 *		\date: Dic 12, 2018
 */

#include <iostream>

#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>

#include "ros/ros.h"
#include "sensor_msgs/Image.h"
#include "geometry_msgs/Pose2D.h"

ros::Publisher _picturePbl, _posePbl;

#define IPADRESS "172.31.1.2" //Camera IP
#define UPD_PORT 9000 //NO idea del puerto yet

using boost::asio::ip::udp;
using boos::asio::ip::adress;

boost::asio::io_service io_service;
udp::socket socket{io_service};

void dispose();

struct Client {
boost::array<char,1024> rx_buffer;
udp::endpoint remote_endpoint;

void handle_rx (const boost::system::error_code& error, size_t bytes_transferred){
if (error){
std::cout<<"Communication failed: " <<error.message() << "\n";
return
}
//test code to know how the buffer array stores the data:
for (int i =rx_buffer.begin(); i<=rx_buffer.begin()+bytes_transferred, i++){
std::cout<<"rx_buffer["i"]=" <<std::string(rx_buffer[i]<< "\n";
}
//std::cout<< "posrecived";
}
void Receiver(){
socket.open(udp::v4());
socket.bind(udp::endpoint(adress::from_string(IPADRESS),UDP_PORT));
socket.async_receive_from(boost::asio::buffer(rx_buffer), remote_endpoint,boost::bind(&Client::handle_rx, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
std::cout <<"recieving data: \n";
io_service.run();
std::cout << "Finish recieving data. \n";
}

int main(int argc, char **argv) {

	//Initialize phase
	try {
		ros::init(argc, argv, "CVision");	//Initializes ROS Node
		
		ROS_INFO("CVision Started...");
		ros::NodeHandle handle;
		_picturePbl = handle.advertise<sensor_msgs::Image>("last_picture", 500, false);
		ROS_INFO("Published last_picture.");
		
		_posePbl = handle.advertise<sensor_msgs::Image>("last_pose", 500, false);
		ROS_INFO("Published last_pose.");
	
	} catch(const std::exception& e) {
		ROS_ERROR("Inintializon error");
		std::cerr << e.what();
		dispose();
		return 1;
	}

	//Run phase
	try {
		while (ros::ok()) {
			/* INSERT YOUR CODE HERE
			 *
			 *
			 *
			 *
			 *
			 *
			 *
			 *
			 *
			 *
			 *
			 **/
			ros::spinOnce();
		}
	} catch(const std::exception& e) {
		ROS_ERROR("Runtime error");
		std::cerr << e.what();
		dispose();
		return 1;
	}

	//Dispose phase
	dispose();
	return 0;
}

void dispose() {
	ROS_INFO("Shutting down...");
	ros::shutdown();
}
