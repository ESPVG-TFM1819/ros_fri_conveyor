/**
 *		
 *		\file: udp_server.hpp
 *		\author: Pablo Morales
 *		\date: Dic 12, 2018
 */

#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>

#include "geometry_msgs/Pose2D.h"

using boost::asio::ip::udp;

namespace cvision {
	/**
	 *	\brief 
	 */
	class udp_server {
		geometry_msgs::Pose2D _pose;

		udp::socket socket_;
		udp::endpoint remote_endpoint_;

		boost::array<char, 1024> recv_buffer_;

		void start_receive(); 
		void handle_receive(const boost::system::error_code& error, std::size_t);
		
		public:
			//\brief Constructor
			udp_server(boost::asio::io_service& io_service);
			geometry_msgs::Pose2D get_pose(); 
	};
}

