/**
 *		
 *		\file Tsk_manager.cpp
 * 		\author Adrià Terrades
 *		\date Oct 24, 2018
 */

#include <stdio.h>

#include "ros/ros.h"
#include "std_msgs/Byte.h"
#include "std_msgs/Float32.h"
#include "conveyor_tech/lbrInfo.h"
#include "conveyor_tech/piecePose.h"

#include "Conveyor.hpp"

using namespace tsk;

Conveyor _conveyor(660, 760);
ros::Publisher _pose;
ros::Subscriber _encoderSub, _poseSub;

void dispose();

void addPiece(const geometry_msgs::Pose2D::ConstPtr& msg) {
	_conveyor.addPiece(msg->x, msg->y, msg->theta);
//	printf("[%.2f, %.2f, %.2f]", msg->x, msg->y, msg->theta);
}

void convCycle(const std_msgs::Float32::ConstPtr& msg) {
	_conveyor.cycle(msg->data);
	_pose.publish(_conveyor.getPiecePose());
}

int main(int argc, char** argv) {

	//Initialize phase
	try {
		ros::init(argc, argv, "tsk_manager");	//Initializes ROS Node
		
		ros::NodeHandle handle;
		_pose = handle.advertise<geometry_msgs::Pose2D>("PiecePose", 500);
		ROS_INFO("Published - PiecePose.");

		_encoderSub = handle.subscribe("encoder_data", 10, convCycle);
		ROS_INFO("Subscribed - encoder_data.");

		_poseSub = handle.subscribe("last_pose", 500, addPiece);
		ROS_INFO("Subscribed - pose_data.");

		ROS_INFO("tsk_manager started.");
	} catch(const std::exception& e) {
		ROS_ERROR("Initialization Error");
		std::cerr << e.what();
		dispose();
		std::exit(100);
	}

	//Run phase
	try {
		while (ros::ok()) {
			ros::spinOnce();
		}
	} catch(const std::exception& e) {
		ROS_ERROR("Runtime Error");
		std::cerr << e.what();
		dispose();
		std::exit(100);
	}

	//Dispose phase
	dispose();
	return 0;
}

void dispose() {
	ROS_INFO("Shutting down...");
	ros::shutdown();
}

