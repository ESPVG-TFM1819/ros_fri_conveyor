/**
 *		\file Conveyor.cpp
 *		\date Dec 04, 2018
 *		\author Adrià Terrades
 */

#include "Conveyor.hpp"

#include <stdio.h>

namespace tsk {

	Conveyor::Conveyor(double width, double lenght, double threshold) : _width(width), _lenght(lenght) {
		//_threshold = lenght * threshold;
		_threshold = 740;
		printf("Threshold: %f\n", _threshold);
	}

	void Conveyor::addPiece(float x, float y, float theta) {
		geometry_msgs::Pose2D piece;
		piece.x = x; piece.y = y; piece.theta = theta;
		addPiece(piece);
	}

	void Conveyor::addPiece(geometry_msgs::Pose2D pose) {
		_pieces.push_back(pose);
		printf("added piece(%d) at %f, %f, %f\n", getNumPieces(), pose.x, pose.y, pose.theta);
	}

	int Conveyor::getNumPieces() {
		return _pieces.size();
	}

	geometry_msgs::Pose2D Conveyor::getPiecePose() {
		if(getNumPieces() <= 0) {
			return geometry_msgs::Pose2D();
		} else {
			return _pieces.front();
		}
	}

	float deltaAns = 0;
	void Conveyor::cycle(float delta) {
		float vel = 0;
		if (delta <= deltaAns) {
			vel = -delta + deltaAns;
		} else {
			vel = (255 - delta) + deltaAns;
		}	
		//vel *= 0.078;
		vel *= 0.072;
		for(auto it = _pieces.begin(); it != _pieces.end();) {
			printf("piece(%.3f, %.3f, %.3f) + %f (%.1f - %.1f)\n", (*it).x, (*it).y, (*it).theta, vel, delta, deltaAns);
			(*it).x += vel;
			if ((*it).x >= _threshold) {
				printf("piece removed (%.3f, %.3f, %.3f), num pieces(%d)\n", (*it).x, (*it).y, (*it).theta, getNumPieces()-1);
				it = _pieces.erase(it);
			} else {
				it++;
			}
		}
		deltaAns = delta;
	}
}
