/**
 * \file Conveyor.hpp
 * \date Dec 04, 2018
 * \author Adrià Terrades
 */

#include <list>

#include "geometry_msgs/Pose2D.h"

namespace tsk {
	/*
	 *	\brief 
	 */
	class Conveyor {
		//\brief set of pieces on the conveyor.
		std::list<geometry_msgs::Pose2D> _pieces;
		double _width, _lenght, _threshold;
		
		public:
			Conveyor(double width, double lenght, double threshold = 0.75);

			//\brief adds a new piece to the conveyor
			void addPiece(float x=0, float y=0, float theta=0);
			//\brief adds a new piece to the conveyor
			void addPiece(geometry_msgs::Pose2D pose);
			//\brief Gets the number of pieces currently on the conveyor.
			int getNumPieces();
			//\brief gets the pose of the priority piece.
			geometry_msgs::Pose2D getPiecePose();
			/**
			 * \brief Runs a cycle of the interpoler
			 * \param delta diference of the between the last lineal motion and the actual.
			 */
			void cycle(float delta);
	}; 
}
